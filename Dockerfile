FROM debian:unstable

RUN apt-get update && apt-get install eatmydata -y && rm -rf /var/lib/apt

RUN apt-get update && eatmydata apt-get install \
    git \
    python3 \
    python3-gitlab \
    python3-docker  \
    -y && rm -rf /var/lib/apt

# Support for docker:dind
ENV DOCKER_HOST=tcp://docker:2375
